# Water cup pyramid
# =================
#
#                 (0, 0)
#             (1, 0)  (1, 1)
#         (2, 0)  (2, 1)  (2, 2)
#     (3, 0)  (3, 1)  (3, 2) (3, 3)
# (4, 0)  (4, 1)  (4, 2) (4, 3)  (4, 4)
#
# Each cup has a volume of one cup. After each cup is full, it starts to
# spill half of the overflow on each side, filling up the cups directly
# below it.
#
# Write a function that gives the amount of water in any given cup after
# a specified number of cups of water is poured in the top of the fountain.
#

class InvalidRowException(ValueError):
    pass

class ToweredGlass():
    """A glass in a given position inside a tower of glasses"""

    def __init__(self, row, column):

        self.target_row=row
        self.target_column=column

    def distribute_water(self, row):
        """
        Return the amount of liquid that overflowed to the taget glass from the
        given row.

        Negatively filled glasses or empty rows are treated as an empty glass.

        Raises InvalidRowException if the given row is below the target glass,
        as it would never fill from the overflow of a row below it.
        """

        if len(row) -1 > self.target_row:
            raise InvalidRowException("The given row is below the target glass")

        next_row = [0]
        for glass in row:
            overflow = (glass-1)/2
            if overflow > 0:
                next_row[-1] += overflow
                next_row.append(overflow)
            else:
                next_row.append(0)
        if len(next_row) - 1 == self.target_row:
            return next_row[self.target_column]
        return self.distribute_water(next_row)

    def pour_from_top(self, glasses):
        """
        Return how much liquid is in the glass if a 'glasses' number of liquid
        is poured from the top of the pyramid
        """

        return self.distribute_water([glasses,])


def get_water_in_cup(row, column, glasses):
    towered_glass = ToweredGlass(row, column)
    return towered_glass.pour_from_top(glasses)


if __name__ == '__main__':
    assert get_water_in_cup(3, 0, 6) == 0
    assert get_water_in_cup(2, 1, 5) == 1
    assert get_water_in_cup(1, 0, 3) == 1
    assert get_water_in_cup(2, 0, 6) == 0.75
    assert get_water_in_cup(3, 2, 6) == 0.25
    assert get_water_in_cup(2, 2, 7) == 1
    assert get_water_in_cup(3, 2, 7) == 0.5
    assert get_water_in_cup(3, 2, 8) == 0.875
    assert get_water_in_cup(6, 1, 15) == 0
    assert get_water_in_cup(3, 0, 0) == 0
    assert get_water_in_cup(3, 0, -1) == 0
    print("Success")
